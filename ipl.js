exports.viratRunsScoredIn2016 = function (matches, deliveries) {
  // By using Higher order function
  const matches1 = matches.reduce((acc, curr)=>{
    if(curr.season==="2016"){
    acc.push(curr.id)
    }
    return acc
  }, [])
  // console.log(matches1)
  const runs = deliveries.reduce((acc, curr)=>{
    if(curr.match_id in matches1 && curr.batsman==="V Kohli"){
      acc+=Number(curr.batsman_runs)
    }
    return acc
  }, 0)
  console.log(runs)
};


exports.totalWicketsTakenByBumrahIn2016 = function (matches, deliveries){
  const matches1 = matches.reduce((acc, curr)=>{
    if(curr.season==="2016"){
    acc.push(curr.id)
    }
    return acc
  }, [])
  // console.log(matches1)
  const wickets = deliveries.reduce((acc, curr)=>{
    if(curr.match_id in matches1 && curr.bowler==="JJ Bumrah" && curr.player_dismissed.length>0){
      acc+=1
      // console.log(curr.player_dismissed, curr.bowler)
    }
    return acc
  }, 0)
  // console.log(wickets)
}


exports.totalBoundariesByMsIn2016 = function(matches, deliveries){
  const matches_id = matches.reduce((acc, curr)=>{
    if(curr.season==="2015"){
    acc.push(curr.id)
    }
    return acc
  }, [])
  const boundaries = deliveries.reduce((acc, curr)=>{
    if(curr.match_id in matches_id && curr.batsman==="MS Dhoni" && curr.batsman_runs==="4"){
      acc+=1
    }
    return acc
  }, 0)
  // console.log(boundaries)
}


exports.totalPlayerBowledByBumrah = function(matches, deliveries){
  const matches_id = matches.reduce((acc, curr)=>{
    if(curr.season==="2015"){
    acc.push(curr.id)
    }
    return acc
  }, [])
  const boundaries = deliveries.reduce((acc, curr)=>{
    if(curr.match_id in matches_id && curr.bowler==="JJ Bumrah" && curr.dismissal_kind==="bowled"){
      acc+=1
    }
    return acc
  }, 0)
  // console.log(boundaries)
}

exports.totalNoBalls = function(matches, deliveries){
  const matches_id = matches.reduce((acc, curr)=>{
    if(curr.season==="2015"){
    acc.push(curr.id)
    }
    return acc
  }, [])
  const boundaries = deliveries.reduce((acc, curr)=>{
    if(curr.match_id in matches_id && curr.bowling_team==="Royal Challengers Bangalore" && curr.noball_runs==="1"){
      acc+=1
      console.log("here")
    }
    return acc
  }, 0)
  console.log(boundaries)
}


