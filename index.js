const fs = require("fs")
const csv = require("csvtojson");
const MATCHES_FILE_PATH = "../data/matches.csv";
const DELIVERIES_FILE_PATH = "../data/deliveries.csv";
const myModule = require('./ipl.js');
const viratScoredIn2016 = myModule.viratRunsScoredIn2016;
let totalWicketsTakenByBumrahIn2016 = myModule.totalWicketsTakenByBumrahIn2016;
const totalBoundariesByMsIn2016 = myModule.totalBoundariesByMsIn2016;
const totalPlayerBowledByBumrah = myModule.totalPlayerBowledByBumrah;
let totalNoBalls = myModule.totalNoBalls;
const JSON_OUTPUT_FILE_PATH = "../output";
function main(){
    csv()
    .fromFile(MATCHES_FILE_PATH)
    .then(matches =>{
        csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then(deliveries=>{
            let viratRuns = viratScoredIn2016(matches, deliveries);
            let BumrahWickets = totalWicketsTakenByBumrahIn2016(matches, deliveries)
            let msBoundaries = totalBoundariesByMsIn2016(matches, deliveries)
            let bumrahBowled = totalPlayerBowledByBumrah(matches, deliveries)
            let totalNoBallsByRcb = totalNoBalls(matches, deliveries)
            
        })     
    })
}


function save_All_Data(value, key){
   const jsonData = {
     [key]: value
   }
   fs.writeFile(JSON_OUTPUT_FILE_PATH+`/${key}.json`, JSON.stringify(jsonData), "utf8", (err)=>{
       if(err){
           console.log(err)
       }
   })
}

main();
